﻿/*  ===== === ==== DungeonMaster ===== === =====
    ===== === ==== DungeonMaster ===== === =====*/
using UnityEngine;
using System.Collections;

public class DungeonMaster :MonoBehaviour{
#region PublicFields
#endregion
#region PrivateFields
#endregion

#region UnityMethods
    private void Awake(){
    }
    private void Start(){
    }
    private void Update(){
    }
#endregion

#region PublicMethods
    public static void ShootHim(string name, string targetName){
        GameObject go =GameObject.Find(targetName);
        Tile t =World.GetTile(go.transform.position);
        bool hit =HitRoll(t.defence);

        if(hit){
            bool damage =DamageRoll();
            if(damage){
                GameObject.FindObjectOfType<EntitiesController>().Kill(targetName);
                if(name[0] =='h')
                    Entities.GetHostile(name).currentTarget ="";
                Debug.Log(targetName + " WASTED");
            }else{
                Debug.Log(name +": It's just flesh wound");
                if(Random.Range(0,2) ==0)
                    Counterstrike(name);
                else
                    Debug.Log("No counterstrike :(");
            }
        } else{
            Debug.Log(name +": Collateral Damage");
            if(Random.Range(0,2) ==0)
                Counterstrike(name);
            else
                Debug.Log(name +": No counterstrike :(");
        }
    }
#endregion
#region PrivateMethods
    private static void Counterstrike(string targetName){
        Vector2 v2 =new Vector2();
        if(targetName[0] =='f')
            v2 =Entities.GetEntity(targetName).position;
        else
            v2 =Entities.GetHostile(targetName).position;
        Tile t =World.GetTile(v2);
        bool hit =HitRoll(t.defence);

        if(hit){
            bool damage =DamageRoll();
            if(damage){
                GameObject.FindObjectOfType<EntitiesController>().Kill(targetName);
                Debug.Log("COUNTER: " +targetName + " WASTED");
            }else{
                Debug.Log("COUNTER: It's just flesh wound");
            }
        } else{
            Debug.Log("COUNTER: Collateral Damage");
        }
    }

    private static bool HitRoll(int mod =0){
        int roll =Random.Range(1, 7);

        if(roll >=3+mod)
            return true;
        else
            return false;
    }
    private static bool DamageRoll(int mod =0){
        int roll =Random.Range(1, 7);

        if(roll >= 4+mod)
            return true;
        else
            return false;
    }
#endregion
}
