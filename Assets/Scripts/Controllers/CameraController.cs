//?/*  ===== === ==== CameraController ===== === =====
//    ===== === ==== CameraController ===== === =====*/
using UnityEngine;
using System.Collections;

public class CameraController :MonoBehaviour{
#region PublicFields
    public float speed =3.0f;
#endregion
#region PrivateFields
#endregion

#region UnityMethods
    private void Awake(){
    }
    private void Start(){
    }
    private void LateUpdate(){
        Vector3 v3 =new Vector3();

        v3.x =Input.GetAxis("Horizontal") *speed *Time.deltaTime;
        v3.y =Input.GetAxis("Vertical") *speed *Time.deltaTime;

        this.transform.localPosition +=v3;
    }
#endregion

#region PublicMethods
#endregion
#region PrivateMethods
#endregion
}
