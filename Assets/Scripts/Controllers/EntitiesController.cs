﻿/*  ===== === ==== EntitiesController ===== === =====
    ===== === ==== EntitiesController ===== === =====*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Factions{friendlies, pings, hostiles};

public class EntitiesController :MonoBehaviour{
#region PublicFields
    public Entity activeEntity {get; protected set;}
#endregion
#region PrivateFields
    private float pingTimer =0.0f;
    private int pingIndex;
    private List<string> pingList;

    private float hostileTime =0.0f;
    private int hostileIndex;
    private List<string> hostileList;
#endregion

#region UnityMethods
    private void Awake(){
        SessionInfo.Instance =null;
        Entities.Instance =null;

        Entities.Instance.LoadEntities(OnEntityPositionChange, OnPingPositionChange);
        activeEntity =null;
        pingIndex =0;
        hostileIndex =0;
    }
    private void Start(){
        InitGraphics();
        SessionInfo.Instance.EndTurn();
    }
    private void Update(){
        if(SessionInfo.Instance.stop)
            return;

        if(SessionInfo.Instance.currentFaction ==Factions.pings)
            MovePings();
        if(SessionInfo.Instance.currentFaction ==Factions.hostiles)
            MoveHostiles();

    }
#endregion

#region PublicMethods
    public void SetActiveEntity(GameObject entityObject){
        WorldController worldController =GameObject.FindObjectOfType<WorldController>();

        if(activeEntity !=null)
            worldController.UnHighlightTiles(activeEntity.movementZonePositions);

        if(entityObject !=null){
            activeEntity =Entities.GetEntity(entityObject.name);

            activeEntity.AP +=4;
            if(activeEntity.AP >5)
                activeEntity.AP =5;

            activeEntity.SetMovementZone(Pathfinder.CalculateMovementZone(activeEntity, activeEntity.AP));

            LookForTargets(activeEntity);
        } else{
            activeEntity =null;
        }

        GameObject.FindObjectOfType<UIController>().UpdateActiveEntityInfo();
    }

    public void LookForTargets(Entity e){
        List<Hostile> targets =new List<Hostile>();

        foreach(Vector2 v2 in e.fov){
            Tile t =World.GetTile(v2);

            foreach(string name in t.occupants){
                if(name[0] =='h')
                    targets.Add(Entities.GetHostile(name));
            }
        }

        e.targets =targets;
        GameObject.FindObjectOfType<UIController>().UpdateTargetsInfo(e.targets);
    }
    public void CorrectOccupantsPosition(Tile t){
        switch (t.occupants.Count){
            case 1:
                GameObject.Find(t.occupants[0]).transform.position =t.position;
                break;
            case 2:
                GameObject.Find(t.occupants[0]).transform.position =t.position +new Vector2(0.25f, 0.25f);
                GameObject.Find(t.occupants[1]).transform.position =t.position +new Vector2(-0.25f, -0.25f);
                break;
            case 3:
                GameObject.Find(t.occupants[0]).transform.position =t.position +new Vector2(0.25f, 0.25f);
                GameObject.Find(t.occupants[1]).transform.position =t.position +new Vector2(-0.25f, -0.25f);
                GameObject.Find(t.occupants[2]).transform.position =t.position +new Vector2(-0.25f, 0.25f);
                break;
            case 4:
                GameObject.Find(t.occupants[0]).transform.position =t.position +new Vector2(0.25f, 0.25f);
                GameObject.Find(t.occupants[1]).transform.position =t.position +new Vector2(-0.25f, -0.25f);
                GameObject.Find(t.occupants[2]).transform.position =t.position +new Vector2(-0.25f, 0.25f);
                GameObject.Find(t.occupants[3]).transform.position =t.position +new Vector2(0.25f, -0.25f);
                break;
        }
    }

    public void Kill(string name){
        if(name[0] =='f')
            KillFriendly(name);
        else
            KillHostile(name);
    }
#endregion
#region PrivateMethods
    #region Init Graphics
    private void InitGraphics(){
        InitEntity();
        InitPing();
    }
    private void InitEntity(){
        for(int i =0; i <Entities.Instance.friendlies.Count; i++){
            Entity e =Entities.Instance.friendlies["fX" +i];


            GameObject go =new GameObject();
            go.name =e.name;
            go.tag ="entity";

            Vector3 position =new Vector3(e.position.x, e.position.y, -0.5f);
            go.transform.position =position;
            go.transform.SetParent(this.transform);

            go.AddComponent<SpriteRenderer>();
            go.GetComponent<SpriteRenderer>().sprite =Resources.Load<Sprite>("Graphics/Sprites/token");
            go.GetComponent<SpriteRenderer>().color =Color.green;
            go.GetComponent<SpriteRenderer>().sortingOrder =8;

            go.AddComponent<BoxCollider2D>();

            SetFov(e, Pathfinder.CalculateFov(e.position));
        }

        foreach(string key in Entities.Instance.friendlies.Keys) {
            Entity e =Entities.Instance.friendlies[key];
            World.Instance.tiles[e.position].AddOccupant(e.name);
        }

    }
    private void InitPing(){
        for(int i =0; i <Entities.Instance.pings.Count; i++){
            Ping p =Entities.Instance.pings["pX" +i];

            World.Instance.tiles[p.position].AddOccupant(p.name);

            GameObject go =new GameObject();
            go.name =p.name;
//			go.tag ="entity";

            Vector3 position =new Vector3(p.position.x, p.position.y, -0.5f);
            go.transform.position =position;
            go.transform.SetParent(this.transform);

            go.AddComponent<SpriteRenderer>();
            go.GetComponent<SpriteRenderer>().sprite =Resources.Load<Sprite>("Graphics/Sprites/ping");
            go.GetComponent<SpriteRenderer>().color =Color.magenta;
            go.GetComponent<SpriteRenderer>().sortingOrder =8;

        }
    }
    private void InitHostile(Hostile h, GameObject go){
        SpriteRenderer sr =go.GetComponent<SpriteRenderer>();
        sr.sprite =Resources.Load<Sprite>("Graphics/Sprites/token");

        if(h.secForce)
            sr.color =Color.red;
        else
            sr.color =Color.yellow;
    }
    #endregion

    private void SetFov(Entity e ,List<Vector2> positions){
        if(e.fov.Count !=0){
            foreach(Vector2 v2 in e.fov)
                World.GetTile(v2).RemoveEntityFromFoV(e);
        }

        e.fov =positions;

        foreach(Vector2 v2 in e.fov){
            Tile t =World.Instance.tiles[v2];
            t.AddEntityToFoV(e);
            CheckOccupantsOf(t);
        }
    }
    private void CheckOccupantsOf(Tile t){
        char prefix;
        if(t.inFoV &&t.occupants.Count !=0){
            for(int j =0; j <t.fovOwners.Count; j++){
                string ownerName =t.fovOwners[j].name;
                prefix =ownerName[0];

                for(int i =0; i <t.occupants.Count; i++){
                    string name =t.occupants[i];

                    if(prefix !=name[0] &&name[0] =='p'){
                        PingSwap(ownerName ,name);
                    }
                }
            }
        }
    }

    private void KillFriendly(string name){
        Entity e =Entities.GetEntity(name);
        ClearEntity(e);

        GameObject.FindObjectOfType<WorldController>().UnHighlightTiles(e.fov);

        GameObject go =GameObject.Find(e.name);
        GameObject.Destroy(go);
    }
    private void KillHostile(string name){
        Hostile h =Entities.GetHostile(name);
        h.UnRegisterPositionChangeCallback(OnHostilePositionChange);
        Tile t =World.GetTile(h.position);

        SessionInfo.Instance.secLevel++;

        t.occupants.Remove(h.name);
        Entities.Instance.hostiles.Remove(name);

        GameObject go =GameObject.Find(name);
        GameObject.Destroy(go);
    }

    #region PingMovement
    private void MovePings(){
        if(pingList ==null)
            pingList =new List<string>(Entities.Instance.pings.Keys);


        if(pingList.Count <=pingIndex){
            pingIndex =0;
            pingList =null;

            SessionInfo.Instance.EndTurn();
            return;
        }

        if(pingTimer <1.0f){
            pingTimer +=Time.deltaTime;
        } else{
            pingTimer =0.0f;

            Debug.Log("Ping: " +pingIndex);
            int mod =Random.Range(1, 3);
            for(int i =0; i <mod; i++){
                if(Entities.Instance.pings.ContainsKey(pingList[pingIndex]) ==false)
                    break;

                Debug.Log("Ping movement steps: " +i +"/" +(mod-1));
                Ping p =Entities.Instance.pings[pingList[pingIndex]];
                int random =Random.Range(0, 2);
                if(random ==0)
                    MovePingNE(p);
                else
                    MovePingSW(p);
            }

            pingIndex+=1;
        }
    }
    private void MovePingNE(Ping p){
        Tile t =World.Instance.tiles[p.position];

        bool n =t.passages[0];
        bool e =t.passages[3];

        if(n &&e ==n){
            int mod =Random.Range(0, 2);

            if(mod ==0)
                e =false;
            else
                n =false;
        }

        if(n)
            p.position =t.position +new Vector2(0, 1);
        else if(e)
            p.position =t.position +new Vector2(1, 0);

    }
    private void MovePingSW(Ping p){
        Tile t =World.Instance.tiles[p.position];

        bool s =t.passages[1];
        bool w =t.passages[2];

        if(s &&w ==s){
            int mod =Random.Range(0, 2);

            if(mod ==0)
                w =false;
            else
                s =false;
        }

        if(s)
            p.position =t.position +new Vector2(0, -1);
        else if(w)
            p.position =t.position +new Vector2(-1, 0);
    }
    #endregion
    private void PingSwap(string entityName, string pingName){
        Ping p =Entities.GetPing(pingName);
        Entities.Instance.pings.Remove(p.name);
        Tile t =World.Instance.tiles[p.position];
        t.RemoveOccupant(p.name);

        Hostile h =new Hostile(p.position);
        h.currentTarget =entityName;
        h.RegisterPositionChangeCallback(OnHostilePositionChange);

        Entities.Instance.hostiles.Add(h.name, h);
        t.AddOccupant(h.name);

        GameObject go =GameObject.Find(pingName);
        go.name =h.name;
        InitHostile(h, go);

        foreach(Entity e in t.fovOwners)
            LookForTargets(e);
    }

    private void MoveHostiles(){
         if(hostileList ==null)
            hostileList =new List<string>(Entities.Instance.hostiles.Keys);

        if(hostileList.Count <=hostileIndex){
            hostileIndex =0;
            hostileList =null;

            SessionInfo.Instance.EndTurn();
            return;
        }

        if(hostileTime <1.0f)
            hostileTime +=Time.deltaTime;
        else{
            Hostile h =Entities.GetHostile(hostileList[hostileIndex]);

            if(h.secForce)
                SecForceAI(h);
            else
                Debug.Log("Worker AI not implemented");

            hostileIndex++;
        }
    }
    private void SecForceAI(Hostile h){
        if(h.currentTarget !="" &&h.fov.Contains(Entities.GetEntity(h.currentTarget).position)) {
            DungeonMaster.ShootHim(h.name, h.currentTarget);
            return;
        } else if(h.currentTarget !=""){
            HuntDownTarget(h);
        } else {
            if(LookForTarget(h) ==false)
                MoveHostile(h);
        }


    }
    #region SecForceAI Elements
    private bool LookForTarget(Hostile h){
        foreach(Vector2 v2 in h.fov){
            Tile t = World.GetTile(v2);

            if(t.occupants.Count !=0){
                List<string> nameList =t.occupants;
                foreach (string name in nameList){
                    if(name[0] =='f'){
                        h.currentTarget =name;
                        DungeonMaster.ShootHim(h.name, name);
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private void MoveHostile(Hostile h){
        Debug.Log("Move Hositle");

        int mod =Random.Range(1, 3);
        for(int i =0; i <mod; i++){
            Debug.Log("Hostile movement steps: " +i +"/" +(mod-1));

            int random =Random.Range(0, 2);
            if(random ==0)
                MoveHostileNE(h);
            else
                MoveHostileSW(h);
        }
    }
    private void MoveHostileNE(Hostile h){
        Tile t =World.Instance.tiles[h.position];

        bool n =t.passages[0];
        bool e =t.passages[3];

        if(n &&e ==n){
            int mod =Random.Range(0, 2);

            if(mod ==0)
                e =false;
            else
                n =false;
        }

        if(n)
            h.position =t.position +new Vector2(0, 1);
        else if(e)
            h.position =t.position +new Vector2(1, 0);

    }
    private void MoveHostileSW(Hostile h){
        Tile t =World.Instance.tiles[h.position];

        bool s =t.passages[1];
        bool w =t.passages[2];

        if(s &&w ==s){
            int mod =Random.Range(0, 2);

            if(mod ==0)
                w =false;
            else
                s =false;
        }

        if(s)
            h.position =t.position +new Vector2(0, -1);
        else if(w)
            h.position =t.position +new Vector2(-1, 0);
    }

    private void HuntDownTarget(Hostile h){
        Entity e =Entities.GetEntity(h.currentTarget);
        List<Vector2> path =Pathfinder.CalculatePath(h.position, e.position);

        int mod =Random.Range(1, 3);
        if(mod >=path.Count)
            mod = path.Count-1;

        h.position =path[mod];

        if(h.AP >=1)
            DungeonMaster.ShootHim(h.name, h.currentTarget);
    }
    #endregion

    #region Callbacks
    private void OnEntityPositionChange(Entity e){
        GameObject entityGO =GameObject.Find(e.name);

        Vector3 position =new Vector3(e.position.x, e.position.y, -0.5f);
        entityGO.transform.position = position;

        if(e.movementZone !=null)
            e.AP -=e.movementZone[e.position];

        SetFov(e, Pathfinder.CalculateFov(e.position));

        e.SetMovementZone(Pathfinder.CalculateMovementZone(e, activeEntity.AP));

        GameObject.FindObjectOfType<UIController>().UpdateActiveEntityInfo();
        LookForTargets(e);

        if(GoalController.goal.Equals(Goals.SearchAndDestroy)) {
            Tile t = World.GetTile(e.position);
            foreach (string name in t.occupants) {
                if (name[0] == 'g')
                    Debug.Log(e.name + " found " + name);
            }
        }
    }
    private void OnPingPositionChange(Ping p){
        GameObject entityGO =GameObject.Find(p.name);

        Vector3 position =new Vector3(p.position.x, p.position.y, -0.5f);
        entityGO.transform.position = position;

        CheckOccupantsOf(World.Instance.tiles[p.position]);
    }
    private void OnHostilePositionChange(Hostile h){
        GameObject entityGO =GameObject.Find(h.name);

        Vector3 position =new Vector3(h.position.x, h.position.y, -0.5f);
        entityGO.transform.position = position;

        h.fov =Pathfinder.CalculateFov(h.position);
        CheckOccupantsOf(World.Instance.tiles[h.position]);
    }
    #endregion

    private void ClearEntity(Entity e){
        e.UnRegisterPositionChangeCallback(OnEntityPositionChange);
        Entities.Instance.friendlies.Remove(e.name);

        Tile tile = World.GetTile(e.position);
        tile.occupants.Remove(e.name);

        foreach (Vector2 v2 in e.fov){
            Tile t =World.GetTile(v2);
            t.RemoveEntityFromFoV(e);
        }
    }
#endregion
}