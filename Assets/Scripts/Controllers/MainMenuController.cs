﻿/*  ===== === ==== MainMenuController ===== === =====
    ===== === ==== MainMenuController ===== === =====*/
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController :MonoBehaviour{
#region PublicFields
#endregion
#region PrivateFields
#endregion

#region UnityMethods
    private void Awake(){
    }
    private void Start(){
    }
    private void Update(){
    }
#endregion

#region PublicMethods
    public void LoadGame(string goal){
        if(goal =="e")
            GoalController.goal =Goals.Exterminate;
        else if(goal == "s")
            GoalController.goal =Goals.SearchAndDestroy;

        SceneManager.LoadScene("GAME");
    }

    public void Quit(){
        Application.Quit();
    }
#endregion
#region PrivateMethods
#endregion
}
