﻿/*  ===== === ==== GoalController ===== === =====
    ===== === ==== GoalController ===== === =====*/
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Goals {Exterminate, SearchAndDestroy};

public class GoalController :MonoBehaviour{
#region PublicFields
    public static Goals goal;
#endregion
#region PrivateFields
    private float _timer;
#endregion

#region UnityMethods
    private void Awake(){
        GameObject.FindObjectOfType<UIController>().ActivateGoalBanner(goal.ToString());
        _timer =0.0f;
    }
    private void Start(){
    }
    private void LateUpdate(){
        if(GameObject.FindObjectOfType<UIController>().goalLabel.Active ==false &&Entities.Instance.friendlies.Count ==0) {
            if (SessionInfo.Instance.stop ==false){
                SessionInfo.Instance.stop =true;
                GameObject.FindObjectOfType<UIController>().ActivateGoalBanner("DEFEAT");
            }

            if(GameObject.FindObjectOfType<UIController>().goalLabel.Active ==false) {
//                SessionInfo.Instance = null;
//                Entities.Instance = null;
                SceneManager.LoadScene("GAME");
                return;
            }
        }
        if(GameObject.FindObjectOfType<UIController>().goalLabel.Active ==false
        &&Entities.Instance.pings.Count ==0 &&Entities.Instance.hostiles.Count ==0){
            if (SessionInfo.Instance.stop ==false){
                SessionInfo.Instance.stop =true;
                GameObject.FindObjectOfType<UIController>().ActivateGoalBanner("VICTORY");
            }

            if(_timer >= 2.0f) {
//                SessionInfo.Instance = null;
//                Entities.Instance = null;
                SceneManager.LoadScene("GAME");
            } else{
                _timer +=Time.deltaTime;
            }
        }
    }
#endregion

#region PublicMethods
#endregion
#region PrivateMethods
    private void CheckExterminationProgress(){

    }
#endregion
}
