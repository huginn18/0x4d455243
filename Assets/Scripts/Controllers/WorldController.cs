﻿/*  ===== === ==== WorldController ===== === =====
    ===== === ==== WorldController ===== === =====*/
using System.Collections.Generic;
using UnityEngine;

public class WorldController :MonoBehaviour{
#region PublicFields
#endregion
#region PrivateFields
#endregion

#region UnityMethods
    private void Awake(){
        World.Instance.LoadWorld(OnFoVStateChange, OnHighlightStateChange);
        InitGraphics();
    }
    private void Start(){
    }
    private void Update(){
    }
#endregion

#region PublicMethods
    public void InitGraphics(){
        foreach(Vector2 v2 in World.Instance.tiles.Keys){
            Tile tile =World.Instance.tiles[v2];

            GameObject tileGO =Instantiate(Resources.Load<GameObject>("Prefabs/Tile"));
            tileGO.name =v2.ToString();

            tileGO.transform.position =v2;
            tileGO.transform.SetParent(this.transform);
            tileGO.GetComponent<TileObject>().InitPassages(tile);
        }

        if(GoalController.goal ==Goals.SearchAndDestroy)
            InitGoals();
    }

    public void HighlightTiles(List<Vector2> positions){
        foreach (Vector2 v2 in positions)
            World.Instance.tiles[v2].highlight =true;
    }
    public void UnHighlightTiles(List<Vector2> positions){
        foreach (Vector2 v2 in positions)
            World.Instance.tiles[v2].highlight =false;
    }


    public TileObject GetTileObject(Vector2 position){
        GameObject go =GameObject.Find(position.ToString());
        if(go ==null)
            Debug.LogError(position);

        return go.GetComponent<TileObject>();
    }
#endregion
#region PrivateMethods
    private void InitGoals(){
        GameObject goal =GameObject.FindObjectOfType<GoalController>().gameObject;
        foreach(Vector2 v2 in World.Instance.goals){

            GameObject goalGO =new GameObject();
            goalGO.name ="goal " +v2;

            goalGO.transform.SetParent(goal.transform);
            goalGO.transform.position =new Vector2(v2.x, v2.y);

            goalGO.AddComponent<SpriteRenderer>();
            goalGO.GetComponent<SpriteRenderer>().sprite =Resources.Load<Sprite>("Graphics/Sprites/goalToken");
            goalGO.GetComponent<SpriteRenderer>().sortingOrder =8;

            World.GetTile(v2).AddOccupant(goalGO.name);
        }
    }

    private void OnHighlightStateChange(Tile tile){
        GetTileObject(tile.position).highlight.SetActive(tile.highlight);
    }
    private void OnFoVStateChange(Tile tile){
        GetTileObject(tile.position).fovMask.SetActive(!tile.inFoV);
    }
#endregion
}
