﻿/*  ===== === ==== MouseController ===== === =====
	===== === ==== MouseController ===== === =====*/
using UnityEngine;

public class MouseController :MonoBehaviour{
#region PublicFields
#endregion
#region PrivateFields
	private EntitiesController entitiesController;
#endregion

#region UnityMethods
	private void Awake(){
	}
	private void Start(){
		entitiesController =GameObject.FindObjectOfType<EntitiesController>();
	}
	private void Update(){
		if(Input.GetMouseButtonDown(0)){
			RaycastHit2D hit;
			hit =Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

			if(hit.collider ==null)
				return;

			if(hit.collider.tag =="tile"){
				if(entitiesController.activeEntity !=null
				&&entitiesController.activeEntity.movementZonePositions.Contains(hit.collider.transform.position))
					entitiesController.activeEntity.position =hit.collider.transform.position;
			}
		}
	}
#endregion

#region PublicMethods
#endregion
#region PrivateMethods
#endregion
}
