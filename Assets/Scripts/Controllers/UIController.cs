﻿/*  ===== === ==== UIController ===== === =====
    ===== === ==== UIController ===== === =====*/
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController :MonoBehaviour{
#region PublicFields
    public Text entityName;
    public Text entityPosition;
    public Text entityActionPoints;

    public Text aTileName;
    public Text aTileDef;
    public Text aTileCap;

    public Text tileName;
    public Text tileDef;
    public Text tileCap;

    public Text turnCounter;

    public FaderObject goalLabel;

    public GameObject targetsContainer;

    public GameObject endTurn;
#endregion
#region PrivateFields
    private List<GameObject> targets;
    private List<GameObject> secLevelBars;
#endregion

#region UnityMethods
    private void Awake(){
        entityName.text ="";
        entityPosition.text ="";
        entityActionPoints.text ="";

        aTileName.text ="";
        aTileCap.text ="";
        aTileDef.text ="";

        targets =new List<GameObject>();
        for(int i =0; i <targetsContainer.transform.childCount; i++){
            targets.Add(targetsContainer.transform.GetChild(i).gameObject);
            targets[i].SetActive(false);
        }

        Transform secLevelContainer =GameObject.Find("Sec Bar").transform;
        secLevelBars =new List<GameObject>();
        for(int i =0; i <secLevelContainer.childCount; i++) {
            secLevelBars.Add(secLevelContainer.GetChild(i).gameObject);
            secLevelBars[i].SetActive(false);
            secLevelBars.Reverse();
        }
    }
    private void Start(){
    }
    private void Update(){
        turnCounter.text = SessionInfo.Instance.turnNumber.ToString();

        if(Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();

        if(Input.GetKeyDown(KeyCode.Return))
            EndTurn();
    }
#endregion

#region PublicMethods
    public void UpdateActiveEntityInfo(){
        EntitiesController ec =GameObject.FindObjectOfType<EntitiesController>();

        if(ec.activeEntity ==null){
            entityName.text ="";
            entityPosition.text ="";
            entityActionPoints.text ="";

            aTileName.text ="";
            aTileCap.text ="";
            aTileDef.text ="";
        } else{
            entityName.text =ec.activeEntity.name;
            entityPosition.text =ec.activeEntity.position.ToString();
            entityActionPoints.text =ec.activeEntity.AP.ToString();

            Tile t =World.Instance.tiles[ec.activeEntity.position];
            aTileName.text =t.position.ToString();
            aTileCap.text ="Cap: " +t.occupantsCap.ToString();
            aTileDef.text ="Def: " +t.defence.ToString();
        }
    }

    public void UpdateTileInfo(Vector2 position){
        Tile t =World.Instance.tiles[position];

        tileName.text =t.position.ToString();
        tileCap.text ="Cap: " +t.occupantsCap.ToString();
        tileDef.text ="Def: " +t.defence.ToString();
    }
    public void ClearTileInfo(){
        tileName.text ="";
        tileCap.text ="";
        tileDef.text ="";
    }

    public void UpdateTargetsInfo(List<Hostile> hostiles){
        for(int c =0; c <targets.Count; c++)
            targets[c].SetActive(false);

        for(int h =0; h <hostiles.Count; h++){
            targets[h].SetActive(true);
            targets[h].GetComponent<TargetObject>().targetName =hostiles[h].name;

            Color color =Color.yellow;
            if(hostiles[h].secForce)
                color =Color.red;

            targets[h].GetComponent<Image>().color =color;
        }
    }

    public void OnSecLevelChange(int secLevel){
        foreach (GameObject go in secLevelBars)
            go.SetActive(false);

        for(int i =0; i <secLevel; i++)
            secLevelBars[i].SetActive(true);
    }

    public void ActivateGoalBanner(string text){
        goalLabel.Active =true;
        goalLabel.GetComponent<Text>().text =text;
    }

    public void EndTurn(){
        EntitiesController ec =GameObject.FindObjectOfType<EntitiesController>();
        ec.SetActiveEntity(null);
        SessionInfo.Instance.EndTurn();
    }

    public void LoadMainMenu(){
        SceneManager.LoadScene("MENU");
    }
#endregion
#region PrivateMethods
#endregion
}
