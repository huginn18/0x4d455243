﻿/*  ===== === ==== World ===== === =====
    ===== === ==== World ===== === =====*/
using System;
using System.Collections.Generic;
using UnityEngine;

public class World {
#region PublicFields
    public static World Instance{
        get{
            if(_instance ==null)
                _instance =new World();

            return _instance;
        }
    }


    public int width, height;

    public Dictionary<Vector2, Tile> tiles;
    public List<Vector2> goals;
#endregion
#region PrivateFields
    private static World _instance;
#endregion

#region PublicMethods
    public void LoadWorld(Action<Tile> cbFoV, Action<Tile> cbHihglight, string name ="demo2"){
        MapData mapData =MapLoader.NewLoadMap(name);

        width =mapData.width;
        height =mapData.height;

        tiles =mapData.tiles;
        foreach(Vector2 v2 in tiles.Keys){
            tiles[v2].RegisterFovStateChangeCallback(cbFoV);
            tiles[v2].RegisterHighlightChangeCallback(cbHihglight);
        }

        if(GoalController.goal ==Goals.SearchAndDestroy) {
            goals = new List<Vector2>();
            List<Vector2> spawnPoints = MapLoader.LoadGoalSpawnPoints("demo2");
            int max = 4;
            if (max > spawnPoints.Count)
                max = spawnPoints.Count;

            for (int i = 0; i < max; i++) {
                int index = UnityEngine.Random.Range(0, spawnPoints.Count);

                Vector2 v2 = spawnPoints[index];
                spawnPoints.Remove(v2);

                goals.Add(v2);
            }
        }
    }
    public static Tile GetTile(Vector2 v2){
        return Instance.tiles[v2];
    }

    public void SetFieldOfVision(Entity e, List<Vector2> positions){
        foreach(Vector2 v2 in positions)
            tiles[v2].inFoV =true;
    }
#endregion
#region PrivateMethods
#endregion
}
