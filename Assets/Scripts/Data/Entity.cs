﻿/*  ===== === ==== Entity ===== === =====
    ===== === ==== Entity ===== === =====*/
using System;
using System.Collections.Generic;
using UnityEngine;

public class Entity {
#region PublicFields
    public string name;
    public Vector2 position{
        get{
            return _position;
        }
        set{
            World.Instance.tiles[position].RemoveOccupant(this.name);

            _position =value;
            _cbPositionChange(this);

            World.Instance.tiles[position].AddOccupant(this.name);
        }
    }
    public int AP =5;

    public List<Vector2> fov;

    public Dictionary<Vector2, int> movementZone {get; protected set;}
    public List<Vector2> movementZonePositions;

    public List<Hostile> targets;
#endregion
#region PrivateFields
    private Vector2 _position;

    private Action<Entity> _cbPositionChange;
#endregion

#region PublicMethods
    public Entity(Vector2 position){
        _position =position;

        fov =new List<Vector2>();
        targets =new List<Hostile>();
    }

    public void SetMovementZone(Dictionary<Vector2, int> zone){
        WorldController wc = GameObject.FindObjectOfType<WorldController>();

        if(movementZone !=null)
            wc.UnHighlightTiles(movementZonePositions);

        movementZonePositions =new List<Vector2>();
        foreach (Vector2 v2 in zone.Keys)
            movementZonePositions.Add(v2);

        movementZone = zone;

        if(movementZone !=null)
            wc.HighlightTiles(movementZonePositions);
    }

    public void RegisterPositionChangeCallback(Action<Entity> cb){
        _cbPositionChange +=cb;
    }
    public void UnRegisterPositionChangeCallback(Action<Entity> cb){
        _cbPositionChange -=cb;
    }
#endregion
#region PrivateMethods
#endregion
}
