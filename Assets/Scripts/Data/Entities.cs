﻿/*  ===== === ==== Entities ===== === =====
    ===== === ==== Entities ===== === =====*/
using System;
using System.Collections.Generic;

public class Entities {
#region PublicFields
    public static Entities Instance{
        get{
            if(_instance ==null) {
	            _instance = new Entities();
            }
            return _instance;
        }
        set{
            _instance =null;
        }
    }

    public Dictionary<string, Entity> friendlies;
    public int friendliesNextID{
        get{
            _friendliesNextID++;
            return _friendliesNextID-1;
        }
    }
    public Dictionary<string, Ping> pings;
    public int pingsNextID{
        get{
            _pingsNextID++;
            return _pingsNextID-1;
        }
    }
    public Dictionary<string, Hostile> hostiles;
    public int hostilesNextID{
        get{
            _hostilesNextID++;
            return _hostilesNextID-1;
        }
    }
#endregion
#region PrivateFields
    private static Entities _instance;

    private int _friendliesNextID =0;
    private int _pingsNextID =0;
    private int _hostilesNextID =0;
#endregion

#region PublicMethods
    public Entities(){
        friendlies =new Dictionary<string, Entity>();
        pings =new Dictionary<string, Ping>();
        hostiles =new Dictionary<string, Hostile>();
    }

    public void LoadEntities(Action<Entity> entityPositionChangeCB, Action<Ping> pingPositionChangeCB, string name ="demo2"){
        EntitiesData entitiesData =MapLoader.LoadEntities(name);

        foreach(Entity e in entitiesData.entities){
            e.RegisterPositionChangeCallback(entityPositionChangeCB);
            friendlies.Add(e.name, e);
        }

        foreach (Ping p in entitiesData.pings){
            p.RegisterPositionChangeCallback(pingPositionChangeCB);
            pings.Add(p.name, p);
        }
    }

    public static Entity GetEntity(string name){
        return Instance.friendlies[name];
    }
    public static Ping GetPing(string name){
        return Instance.pings[name];
    }
    public static Hostile GetHostile(string name){
        return Instance.hostiles[name];
    }
#endregion
#region PrivateMethods
#endregion
}
