﻿/*  ===== === ==== Hostile ===== === =====
    ===== === ==== Hostile ===== === =====*/
using System;
using System.Collections.Generic;
using UnityEngine;

public class Hostile {
#region PublicFields
    public string name;
    public Vector2 position{
        get{
            return _position;
        }
        set{
            World.Instance.tiles[position].RemoveOccupant(this.name);

            _position =value;
            _cbPositionChange(this);

            World.Instance.tiles[position].AddOccupant(this.name);
        }
    }
    public int AP;

    public bool secForce =true;

    public List<Vector2> fov;

    public string currentTarget ="";
#endregion
#region PrivateFields
    private Vector2 _position;

    private Action<Hostile> _cbPositionChange;
#endregion

#region PublicMethods
    public Hostile(Vector2 position){
        _position =position;
        fov =Pathfinder.CalculateFov(_position);

        name ="hX" +Entities.Instance.hostilesNextID;
        AP =5;

        int mod = UnityEngine.Random.Range(0,2);
        if(mod ==0)
            secForce =false;
    }

    public void RegisterPositionChangeCallback(Action<Hostile> cb){
        _cbPositionChange +=cb;
    }
    public void UnRegisterPositionChangeCallback(Action<Hostile> cb){
        _cbPositionChange -=cb;
    }
#endregion
#region PrivateMethods
#endregion
}
