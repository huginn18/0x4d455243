﻿/*  ===== === ==== Tile ===== === =====
    ===== === ==== Tile ===== === =====*/

using System;
using System.Collections.Generic;
using UnityEngine;

public class Tile{
#region PublicFields
    public Vector2 position {get; protected set;}

    public List<bool> passages;
    public List<bool> fovPassages;
    public List<string> occupants{
        get{
            return _occupants;
        }
    }

    public int occupantsCap {get; protected set;}
    public int defence {get; protected set;}
    public int hitPoints {get; protected set;}

    public bool highlight {
        get{
            return _highlight;
        }
        set{
            _highlight =value;

//			if(cbHighlightStateChange ==null)
//				Debug.LogError("HighlightStateChangeCb == null");
//			else
                _cbHighlightStateChange(this);
        }
    }
    public bool inFoV {
        get{
            return _fovMask;
        }
        set{
            _fovMask =value;

            if(_cbFoVStateChange ==null)
                Debug.LogError("FovStateChangeCb ==null");
            else
                _cbFoVStateChange(this);
        }
    }
    public List<Entity> fovOwners {get; protected set;}


#endregion
#region PrivateFields
    private bool _highlight;
    private bool _fovMask;

    private List<string> _occupants;

    private Action<Tile> _cbFoVStateChange;
    private Action<Tile> _cbHighlightStateChange;
#endregion

#region PublicMethods
    public Tile(Vector2 position){
        this.position =position;

        occupantsCap =UnityEngine.Random.Range(1, 5);
        defence =UnityEngine.Random.Range(0, 3);
        hitPoints =1;

        _highlight =false;
        _fovMask =true;

        fovOwners =new List<Entity>();
        _occupants =new List<string>();

        fovPassages =new List<bool>();
        passages =new List<bool>();
        for(int i =0; i <4; i++) {
            passages.Add(false);
            fovPassages.Add(false);
        }
    }

    public void AddEntityToFoV(Entity e){
        fovOwners.Add(e);

        inFoV =true;
    }
    public void RemoveEntityFromFoV(Entity e){
        fovOwners.Remove(e);

        if(fovOwners.Count ==0)
            inFoV =false;
    }

    public void AddOccupant(string occupant){
        _occupants.Add(occupant);

        if(_occupants.Count >1)
            GameObject.FindObjectOfType<EntitiesController>().CorrectOccupantsPosition(this);

    }
    public void RemoveOccupant(string occupant){
        int old =_occupants.Count;
        _occupants.Remove(occupant);

        if(old >1 &&_occupants.Count ==1)
            GameObject.FindObjectOfType<EntitiesController>().CorrectOccupantsPosition(this);
    }

    #region Callbacks
    public void RegisterFovStateChangeCallback(Action<Tile> cb){
        _cbFoVStateChange +=cb;
    }
    public void UnRegisterFovStateChangeCallback(Action<Tile> cb){
        _cbFoVStateChange -=cb;
    }

    public void RegisterHighlightChangeCallback(Action<Tile> cb){
        _cbHighlightStateChange +=cb;
    }
    public void UnRegisterHighlightChangeCallback(Action<Tile> cb){
        _cbHighlightStateChange -=cb;
    }
    #endregion
#endregion
#region PrivateMethods

#endregion
}
