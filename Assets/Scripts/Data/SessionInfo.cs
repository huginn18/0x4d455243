﻿/*  ===== === ==== Turn ===== === =====
    ===== === ==== Turn ===== === =====*/
using System.Collections.Generic;
using UnityEngine;

public class SessionInfo {
#region PublicFields
    public static SessionInfo Instance{
        get{
            if(_instance ==null) {
                _instance = new SessionInfo();
            }
            return _instance;
        }
        set{
            _instance =value;
        }
    }

    public Factions currentFaction {get; protected set;}
    public int turnNumber {get; protected set;}
    public int secLevel{
        get{
            return _secLevel;
        }
        set{
			_secLevel =value;
            if(secLevel <0)
                secLevel =0;
            if(secLevel >10)
                secLevel =10;

            GameObject.FindObjectOfType<UIController>().OnSecLevelChange(secLevel);
        }
    }

    public bool stop;
#endregion
#region PrivateFields
    private static SessionInfo _instance;
    private int index =0;

    private int _secLevel =0;
#endregion

#region PublicMethods
    public SessionInfo(){
        stop =false;
        currentFaction =Factions.friendlies;
        turnNumber =1;
    }

    public void EndTurn(){
        if(stop)
            return;

        if(currentFaction !=Factions.friendlies
        || index ==Entities.Instance.friendlies.Count)
            NextFaction();

        if(currentFaction ==Factions.friendlies)
            NextUnit();
    }
#endregion
#region PrivateMethods
    private void NextFaction(){
        switch(currentFaction){
            case Factions.friendlies:
                currentFaction =Factions.pings;
                turnNumber++;
                break;
            case Factions.pings:
                currentFaction =Factions.hostiles;
                break;
            case Factions.hostiles:
                currentFaction =Factions.friendlies;
                break;
        }

        index =0;
    }
    private void NextUnit(){
        List<string> list =new List<string>(Entities.Instance.friendlies.Keys);

        string name =list[index];
        EntitiesController ec =GameObject.FindObjectOfType<EntitiesController>();
        ec.SetActiveEntity(GameObject.Find(name));

        index+=1;
    }
#endregion
}
