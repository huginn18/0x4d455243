﻿/*  ===== === ==== Ping ===== === =====
	===== === ==== Ping ===== === =====*/
using System;
using UnityEngine;

public class Ping {
#region PublicFields
	public string name;
	public Vector2 position{
		get{
			return _position;
		}
		set{
			World.Instance.tiles[position].RemoveOccupant(this.name);
			_position =value;
			World.Instance.tiles[position].AddOccupant(this.name);

			_cbPositionChange(this);
		}
	}
#endregion
#region PrivateFields
	private Vector2 _position;

	private Action<Ping> _cbPositionChange;
#endregion

#region PublicMethods
	public Ping(Vector2 position){
		_position =position;
	}

	public void RegisterPositionChangeCallback(Action<Ping> cb){
		_cbPositionChange +=cb;
	}
	public void UnRegisterPositionChangeCallback(Action<Ping> cb){
		_cbPositionChange -=cb;
	}
#endregion
#region PrivateMethods
#endregion
}
