﻿/*  ===== === ==== MapLoader ===== === =====
    ===== === ==== MapLoader ===== === =====*/
using System;
using System.Collections.Generic;
using UnityEngine;

public class MapLoader {
#region PublicFields
#endregion
#region PrivateFields
#endregion

#region PublicMethods
    public static MapData LoadMap(string name){
        MapLayer layer =LoadLayer(name, "T");

        MapData data=new MapData();
        data.width =layer.width;
        data.height =layer.height;
        data.tiles =ParseTiles(layer);

        return data;
    }
    public static MapData NewLoadMap(string name){
        MapLayer layer =LoadLayer(name, "T");

        MapData data=new MapData();
        data.width =layer.width;
        data.height =layer.height;
        data.tiles =NewParseTiles(layer);

        return data;
    }

    public static EntitiesData LoadEntities(string name){
        MapLayer layer =LoadLayer(name, "E");

        return ParseEntities(layer);
    }
    public static EntitiesData NewLoadEntities(string name){
        MapLayer layer =LoadLayer(name, "E");

        return ParseEntities(layer);
    }

    public static List<Vector2> LoadGoalSpawnPoints(string name){
        MapLayer layer =LoadLayer(name, "G");

        return ParseGoalSpawnPoints(layer);
    }
#endregion
#region PrivateMethods
    private static MapLayer LoadLayer(string name, string suffix){
        Texture2D img =Resources.Load<Texture2D>("Graphics/Maps/"+name+suffix);

        int width =img.width;
        int height =img.height;

        Color32[] pixels =img.GetPixels32();

        return new MapLayer(width, height, pixels);
    }


    private static Dictionary<Vector2, Tile> ParseTiles(MapLayer layer){
        Dictionary<Vector2, Tile> tiles =new Dictionary<Vector2, Tile>();
        for(int x =0; x <layer.width; x++){
            for(int y =0; y <layer.height; y++){
                Color32 pixel =layer.GetPixel(x, y);

                if(pixel.Equals(new Color32(255, 0, 255, 255)))
                    continue;

                List<bool> passages =new List<bool>();
                for(int i =0; i <4; i++)
                    passages.Add(false);

                if(pixel.g ==128)
                    passages[0] =true;
                else if(pixel.g ==64)
                    passages[1] =true;
                else if(pixel.g ==255){
                    passages[0] =true;
                    passages[1] =true;
                }

                if(pixel.b ==128)
                    passages[2] =true;
                else if(pixel.b ==64)
                    passages[3] =true;
                else if(pixel.b ==255){
                    passages[2] =true;
                    passages[3] =true;
                }

                Tile tile =new Tile(new Vector2(x, y));
                tile.passages =passages;
                tiles.Add(new Vector2(x, y), tile);
            }
        }

        return tiles;
    }
    private static Dictionary<Vector2, Tile> NewParseTiles(MapLayer layer){
        Dictionary<Vector2, Tile> tiles =new Dictionary<Vector2, Tile>();
        for(int x =0; x <layer.width; x+=2){
            for(int y =0; y <layer.height; y+=2){
                Color32 pixel =layer.GetPixel(x, y);

                if(pixel.Equals(new Color32(255, 0, 255, 255)))
                    continue;

                List<bool> passages =new List<bool>();
                for(int i =0; i <4; i++)
                    passages.Add(false);

                if(pixel.g ==128)
                    passages[0] =true;
                else if(pixel.g ==64)
                    passages[1] =true;
                else if(pixel.g ==255){
                    passages[0] =true;
                    passages[1] =true;
                }

                if(pixel.b ==128)
                    passages[2] =true;
                else if(pixel.b ==64)
                    passages[3] =true;
                else if(pixel.b ==255){
                    passages[2] =true;
                    passages[3] =true;
                }

                List<bool> fovList =CheckFoVPassages(layer, new Vector2(x, y));

                Tile tile =new Tile(new Vector2(x/2, y/2));
                tile.passages =passages;
                tile.fovPassages =fovList;
                tiles.Add(new Vector2(x/2, y/2), tile);
            }
        }

        return tiles;
    }

    private static List<bool> CheckFoVPassages(MapLayer layer, Vector2 position){
        List<bool> list =new List<bool>();

        for(int i =0; i < 4; i++){
            Vector2 pos =position;
            switch (i){
                case 0:
                    pos+=new Vector2(0, 1);
                    break;
                case 1:
                    pos+=new Vector2(0, -1);
                    break;
                case 2:
                    pos+=new Vector2(-1, 0);
                    break;
                case 3:
                    pos+=new Vector2(1, 0);
                    break;
            }

            Color32 pixel =layer.GetPixel((int)pos.x, (int)pos.y);
            if(pixel.Equals(new Color32(255, 0, 255, 255)))
                list.Add(true);
            else
                list.Add(false);
        }

        return list;
    }
    private static EntitiesData ParseEntities(MapLayer layer){
        List<Entity> entities =new List<Entity>();
        List<Ping> pings =new List<Ping>();

        for(int x =0; x <layer.width; x+=2){
            for(int y =0; y <layer.height; y+=2){
                Color32 pixel =layer.GetPixel(x, y);

                if(pixel.Equals(new Color32(255, 0, 255, 255)))
                    continue;

                if(pixel.Equals(new Color32(255, 0, 0, 255))){
                    Ping p =new Ping(new Vector2(x/2, y/2));
                    p.name ="pX" +pings.Count;

                    pings.Add(p);
                } else if(pixel.g.Equals(255)){
                    for(int i =0; i <pixel.b; i++) {
                        Entity e = new Entity(new Vector2(x / 2, y / 2));
                        e.name = "fX" + entities.Count;

                        entities.Add(e);
                    }
                }
            }
        }

        EntitiesData entitiesData =new EntitiesData();
        entitiesData.entities =entities;
        entitiesData.pings =pings;

        return entitiesData;
    }

    private static List<Vector2> ParseGoalSpawnPoints(MapLayer layer){
        List<Vector2> spawnPoints =new List<Vector2>();

        for(int x =0; x < layer.width; x++){
            for(int y =0; y <layer.height; y++){
                Color32 pixel =layer.GetPixel(x, y);

                if(pixel.Equals(new Color32(255, 0, 255, 255)))
                    continue;

                if(pixel.Equals(new Color32(0 , 255, 0, 255)))
                    spawnPoints.Add(new Vector2(x /2, y /2));
            }
        }

        return spawnPoints;
    }
#endregion
}


public struct MapLayer{
    public int width;
    public int height;

    public Color32[] pixels;

    public MapLayer(int w, int h, Color32[] c){
        this.width =w;
        this.height =h;

        this.pixels=c;
    }

    public Color32 GetPixel(int x, int y){
        if(x <=-1 || y <=-1 || x >=width ||y >=height) {
            return new Color32(255, 0, 255, 255);
        }

        return pixels[x +(y *width)];
    }
}
public struct MapData{
    public int width, height;

    public Dictionary<Vector2, Tile> tiles;
}
public class EntitiesData{
    public List<Entity> entities;
    public List<Ping> pings;

    public EntitiesData(){
        entities =new List<Entity>();
        pings =new List<Ping>();
    }
}