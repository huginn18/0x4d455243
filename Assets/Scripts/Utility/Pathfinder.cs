﻿/*  ===== === ==== Pathfinder ===== === =====
    ===== === ==== Pathfinder ===== === =====*/
using System.Collections.Generic;
using UnityEngine;

public class Pathfinder {
#region PublicFields
#endregion
#region PrivateFields
#endregion

#region PublicMethods
    public static List<Vector2> CalculateFov(Vector2 position){
        List<Vector2> positions =new List<Vector2>();
        Tile t =World.Instance.tiles[position];
        positions.Add(position);

        for(int i =0; i <4; i++){
            if(t.fovPassages[i]){
                switch(i){
                    case 0:
                        positions.AddRange(CheckLine(position, 0, new Vector2(0, 1)));
                        break;
                    case 1:
                        positions.AddRange(CheckLine(position, 1, new Vector2(0, -1)));
                        break;
                    case 2:
                        positions.AddRange(CheckLine(position, 2, new Vector2(-1, 0)));
                        break;
                    case 3:
                        positions.AddRange(CheckLine(position, 3, new Vector2(1, 0)));
                        break;
                }
            }
        }

        return positions;
    }
    public static Dictionary<Vector2, int> CalculateMovementZone(Entity e, int maxCost){
        Graph graph =new Graph();
        List<Node> open =new List<Node>();
        open.Add(graph.nodes[e.position]);

        List<Node> close =new List<Node>();
        while(open.Count !=0){
            Node currentNode =open[0];
            open.Remove(currentNode);
            close.Add(currentNode);

            foreach(Node n in currentNode.neighbours){
                if(close.Contains(n))
                    continue;

                if(open.Contains(n) &&n.costSoFar >currentNode.costSoFar +1)
                    n.costSoFar =currentNode.costSoFar++;
                else if(open.Contains(n) ==false &&maxCost >=currentNode.costSoFar +1){
                    n.costSoFar =currentNode.costSoFar +1;
                    open.Add(n);
                }
            }
        }

        Dictionary<Vector2, int> positions =new Dictionary<Vector2, int>();
        foreach(Node n in close)
            positions.Add(n.position, n.costSoFar);

        return positions;
    }

    public static int GetDistance(Vector2 a, Vector2 b){
        Graph graph =new Graph();
        List<Node> open =new List<Node>();
        open.Add(graph.nodes[a]);

        List<Node> close =new List<Node>();
        while(open.Count !=0){
            Node currentNode =open[0];
            open.Remove(currentNode);
            close.Add(currentNode);

            if(currentNode.position ==b)
                return currentNode.costSoFar;

            foreach(Node n in currentNode.neighbours){
                if(close.Contains(n))
                    continue;

                if(open.Contains(n) &&n.costSoFar >currentNode.costSoFar +1)
                    n.costSoFar =currentNode.costSoFar++;
                else {
                    n.costSoFar =currentNode.costSoFar +1;
                    open.Add(n);
                }
            }
        }
        return -1;
    }
    public static List<Vector2> CalculatePath(Vector2 a, Vector2 b){
        Graph graph =new Graph();
        List<Node> open =new List<Node>();
        open.Add(graph.nodes[a]);

        List<Node> close =new List<Node>();
        while(open.Count !=0){
            Node currentNode =FindCheapestNode(open);
            open.Remove(currentNode);
            close.Add(currentNode);

            if(currentNode.position ==b)
                return CalculatePath(graph.nodes[a], currentNode);

            foreach(Node n in currentNode.neighbours){
                if(close.Contains(n))
                    continue;

                if(open.Contains(n) &&n.costSoFar >currentNode.costSoFar +1) {
                    n.costSoFar = currentNode.costSoFar++;
                    n.parent =currentNode;
                }
                else {
                    n.costSoFar =currentNode.costSoFar +1;
                    n.parent =currentNode;
                    open.Add(n);
                }
            }
        }

        return null;
    }
#endregion
#region PrivateMethods
    private static List<Vector2> CheckLine(Vector2 position, int passageIndex, Vector2 vector){
        List<Vector2> validPositions =new List<Vector2>();

        bool flag =true;
        for(int i =0; flag; i++){
            Vector2 pos =new Vector2(position.x +vector.x*i, position.y +vector.y*i);

            if(World.Instance.tiles.ContainsKey(pos) ==false)
                flag =false;
            else if(World.Instance.tiles[pos].fovPassages[passageIndex] ==false){
                flag =false;
                validPositions.Add(pos);
            } else
                validPositions.Add(pos);
        }

        return validPositions;
    }

    private static List<Vector2> CalculatePath(Node start, Node end){
        List<Vector2> path =new List<Vector2>();
        Node node =end;
        Debug.Log(end.position);
        Debug.Log(node.position);
        while(node !=start){
            Vector2 v2 =node.position;
            path.Add(v2);
            node =node.parent;
        }
        path.Reverse();

        return path;
    }
    private static Node FindCheapestNode(List<Node> nodes){
        Node node =nodes[0];
        foreach (Node n in nodes){
            if(n.costSoFar <node.costSoFar)
                node =n;
        }

        return node;
    }
#endregion
}

public class Graph{
    public int w {get; protected set;}
    public int h {get; protected set;}

    public Dictionary<Vector2, Node> nodes {get; protected set;}

    public Graph(){
        w =World.Instance.width;
        h =World.Instance.height;

        nodes =new Dictionary<Vector2, Node>();
        foreach (Vector2 v2 in World.Instance.tiles.Keys){
            Tile t =World.Instance.tiles[v2];

            Node node =new Node(this, t);
            nodes.Add(v2, node);
        }
    }
}
public class Node{
    public Graph graph;

    public Vector2 position;

    public List<bool> passages;
    public List<Node> neighbours{
        get{
            if(_neighbours ==null){
                _neighbours =new List<Node>();
                FindNeighbours();
            }

            return _neighbours;
        }
    }
    private List<Node> _neighbours;

    public int costSoFar;
    public Node parent;

    public Node(Graph g,Tile t){
        graph =g;

        position =t.position;
        passages =t.passages;

        costSoFar =0;
    }

    private void FindNeighbours(){
        for(int i =0; i <4; i++){
            if(passages[i]){
                Vector2 pos =this.position;
                switch (i){
                    case 0:
                        pos+=new Vector2(0, 1);
                        break;
                    case 1:
                        pos+=new Vector2(0, -1);
                        break;
                    case 2:
                        pos+=new Vector2(-1, 0);
                        break;
                    case 3:
                        pos+=new Vector2(1, 0);
                        break;
                }

                if(pos !=this.position && graph.nodes.ContainsKey(pos))
                   neighbours.Add(graph.nodes[pos]);
            }
        }
    }
}
