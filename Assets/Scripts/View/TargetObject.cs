﻿/*  ===== === ==== TargetObject ===== === =====
	===== === ==== TargetObject ===== === =====*/
using UnityEngine;
using UnityEngine.EventSystems;

public class TargetObject :MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler{
#region PublicFields
	public string targetName;
#endregion
#region PrivateFields
#endregion

#region UnityMethods
	private void Awake(){
	}
	private void Start(){
	}
	private void Update(){
	}

	public void OnPointerEnter(PointerEventData eventData){
		GameObject.Find(targetName).transform.localScale =new Vector2(1.25f, 1.25f);
	}
	public void OnPointerExit(PointerEventData eventData){
		GameObject.Find(targetName).transform.localScale =Vector2.one;
	}

	public void OnPointerClick(PointerEventData eventData){
		EntitiesController ec =GameObject.FindObjectOfType<EntitiesController>();

		if(ec.activeEntity.AP !=0){
			DungeonMaster.ShootHim(ec.activeEntity.name, targetName);

			ec.activeEntity.AP-=1;
			ec.LookForTargets(ec.activeEntity);
			ec.activeEntity.SetMovementZone(Pathfinder.CalculateMovementZone(ec.activeEntity, ec.activeEntity.AP));

			GameObject.FindObjectOfType<UIController>().UpdateActiveEntityInfo();
		} else
			Debug.Log("Not enough AP");
	}
#endregion

#region PublicMethods
#endregion
#region PrivateMethods
#endregion
}
