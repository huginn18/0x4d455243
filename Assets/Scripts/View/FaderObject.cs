﻿/*  ===== === ==== FaderObject ===== === =====
    ===== === ==== FaderObject ===== === =====*/
using UnityEngine;
using UnityEngine.UI;

public class FaderObject :MonoBehaviour{
#region PublicFields
    public bool Active {
        get{
            return _active;
        }
        set{
            _active =value;
            this.GetComponent<Text>().enabled =true;
            this.GetComponent<Text>().color = new Color(0.5058824f, 0.007843138f, 0.08627451f);
        }
    }
#endregion
#region PrivateFields
    private float _timer;
    private bool _active;
#endregion

#region UnityMethods
    private void Awake(){
        Active =true;
    }
    private void Start(){
    }
    private void Update(){
        if(_active){
            Color c =this.GetComponent<Text>().color;
            this.GetComponent<Text>().color =new Color(c.r, c.g, c.b, c.a -Time.deltaTime/4);

            if(this.GetComponent<Text>().color.a <=Time.deltaTime) {
                this.GetComponent<Text>().enabled =false;
                _active =false;
            }
        }
    }
#endregion

#region PublicMethods
#endregion
#region PrivateMethods
#endregion
}
