﻿/*  ===== === ==== TileController ===== === =====
    ===== === ==== TileController ===== === =====*/
using UnityEngine;
using System.Collections;

public class TileObject :MonoBehaviour{
#region PublicFields
    public GameObject n, s, w, e;
    public GameObject dN, dS, dW, dE;
    public GameObject fovMask;
    public GameObject highlight;
#endregion
#region PrivateFields
    private UIController uiController;
#endregion

#region UnityMethods
    private void Awake(){
        uiController =GameObject.FindObjectOfType<UIController>();
    }
    private void Start(){
    }
    private void Update(){
    }

    private void OnMouseEnter(){
        uiController.UpdateTileInfo(this.transform.position);
    }
    private void OnMouseExit(){
        uiController.ClearTileInfo();
    }
#endregion

#region PublicMethods
    public void InitPassages(Tile tile){
        n.SetActive(tile.passages[0]);
        s.SetActive(tile.passages[1]);
        w.SetActive(tile.passages[2]);
        e.SetActive(tile.passages[3]);

        if(tile.passages[0] ==true)
            dN.SetActive(!tile.fovPassages[0]);
        if(tile.passages[1] ==true)
            dS.SetActive(!tile.fovPassages[1]);
        if(tile.passages[2] ==true)
            dW.SetActive(!tile.fovPassages[2]);
        if(tile.passages[3] ==true)
            dE.SetActive(!tile.fovPassages[3]);
    }
#endregion
#region PrivateMethods
#endregion
}
